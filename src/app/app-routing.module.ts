import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { LoginComponent } from './pages/login/login.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';

import { PacienteComponent } from './pages/paciente/paciente.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { MedicoEdicionComponent } from './pages/medico/medico-edicion/medico-edicion.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { ConsultaEdicionComponent } from './pages/consulta/consulta-edicion/consulta-edicion.component';


const routes: Routes = [
{ path:"examen",component:ExamenComponent,children:[
  {
    path:'edicion/:id',component:ExamenEdicionComponent
  },
  {
    path:'nuevo',component:ExamenEdicionComponent
  }
]
},

{ path:"especialidad",component:EspecialidadComponent, children:[
  {
    path:'edicion/:id',component:EspecialidadEdicionComponent
  },
  {
    path:'nuevo',component:EspecialidadEdicionComponent
  }
]
},
{ path:"paciente",component:PacienteComponent, children:[
  {
    path:'edicion/:idPaciente',component:PacienteEdicionComponent
  },
  {
    path:'nuevo',component:PacienteEdicionComponent
  }
]
},
{ path:"medico",component:MedicoComponent, children:[
  {
    path:'edicion/:id',component:MedicoEdicionComponent
  },
  {
    path:'nuevo',component:MedicoEdicionComponent
  }
]
},
{ path:"consulta",component:ConsultaComponent, children:[
  {
    path:'nuevo',component:ConsultaEdicionComponent
  },
  {
    path:'edicion/:id',component:ConsultaEdicionComponent
  }
]
},
{path:'login',component:LoginComponent}, 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
