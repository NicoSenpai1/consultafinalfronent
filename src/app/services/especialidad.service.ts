import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST, TOKEN_NAME } from '../shared/var.constant';
import { Subject } from 'rxjs';
import { Especialidad } from '../model/especialidad';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {
  especialidadCambio=new Subject<Especialidad[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/especialidad`;
  constructor(private http:HttpClient) { }
  listar(){  
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;  
    return this.http.get<Especialidad[]>(this.url,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });

  }
  modificar(especialidad:Especialidad){    
    console.log(this.url);
    console.log(especialidad);
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(this.url,especialidad,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
  registar(especialidad:Especialidad){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(this.url,especialidad,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
  listarPorId(id:number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Especialidad>(`${this.url}/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });    
  }
  eliminar(id:number){   
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token; 
    return this.http.delete(`${this.url}/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });
  }
}
