import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatSidenavModule, MatButtonModule, MatMenuModule, MatDividerModule, MatTableModule, MatInputModule, MatTableDataSource, MatCardModule, MatSnackBarModule, MatDatepickerModule, MatAutocompleteModule, MatListModule, MatExpansionModule, MAT_DATE_LOCALE, MatNativeDateModule, MatSelectModule, MatPaginatorModule, MatPaginatorIntl, MatDialogModule, MatProgressBarModule } from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';


@NgModule({

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatTableModule,    
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatListModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatSelectModule,
    MatPaginatorModule,
    MatDialogModule,
    MatProgressBarModule,
    MatGridListModule

  ],
  exports:[
        MatToolbarModule,
        MatSidenavModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatDividerModule,
        MatTableModule,
        MatInputModule,
        MatCardModule,
        MatDatepickerModule,
        MatAutocompleteModule,
        MatListModule,
        MatExpansionModule,
        MatNativeDateModule,
        MatSelectModule,
        MatPaginatorModule,
        MatDialogModule,
        MatProgressBarModule,
        MatGridListModule
  
],
  providers: [
    {
    provide: MAT_DATE_LOCALE,useValue:'es-ES'
  },
  /*{
    provide: MatPaginatorIntl, useClass: MatPaginatorImpl
  },*/
]
  
})export class MaterialModule { }