import { Especialidad } from './especialidad';
import { DetalleConsulta } from './detalleconsulta';
import { Paciente } from './paciente';
import { Medico } from "./medico";

export class Consulta{            
    id:number;
    paciente=new Paciente();
    fecha:string;
    medico=new Medico();
    especialidad=new Especialidad();
    detalleConsulta: DetalleConsulta[]=[];
}