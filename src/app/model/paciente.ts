export class Paciente{
    idPaciente:number
	nombrePaciente:string
	rutPaciente:string
    email:string
    numTelefono:string
    tipoSangre:string
    fecha:string
}