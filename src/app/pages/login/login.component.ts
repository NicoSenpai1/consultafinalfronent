import { LoginService } from './../../services/login.service';
import { catchError } from 'rxjs/operators';
import { TOKEN_NAME } from './../../shared/var.constant';
import * as decode from 'jwt-decode';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import '../login-animation.js';
import { MenuService } from 'src/app/services/menu.service';

import { throwError } from 'rxjs';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',  
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string;
  clave: string;
  mensaje: string = "";
  error: string = "";
  constructor(private loginService: LoginService,private router: Router,private menuService: MenuService){}
  //constructor(private loginService: LoginService, private menuService: MenuService, private router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    (window as any).initialize();
  }
  iniciarSesion2(){
    //console.log(this.usuario);
    //console.log(this.clave);
    try{
      this.loginService.login(this.usuario, this.clave).subscribe(data => {      
      if (data) {
        let token = JSON.stringify(data);
        sessionStorage.setItem(TOKEN_NAME, token);
        let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
        const decodedToken = decode(tk.access_token);
        
        this.menuService.listarPorUsuario(this.usuario).subscribe(data => {
          this.menuService.menuCambio.next(data);
        });

        this.router.navigate(["consulta"]);
      }
      else{
        this.mensaje=`Usuario y Contraseña invalido`;
      }
  });
}
catch(error){
  this.mensaje=`Usuario y Contraseña invalido`;
}
}

iniciarSesion(){
  //console.log(this.usuario);
  //console.log(this.clave);
  try{
    
    this.loginService.login(this.usuario, this.clave).pipe(
      catchError(error=>{
        console.log('Handling error locally and rethrowing it...', error);
        if(this.usuario===""|| this.clave===""){
          this.mensaje=`Usuario y Contraseña invalido`;  
        }
        this.mensaje=`Usuario y Contraseña invalido`;
        return throwError(error);
      })).subscribe(/*res=>console.log('Http Response ',res),
          err=>console.log('Http Response ',err),
          ()=>console.log('Http request Complete'),*/
          data => {      
            if (data) {
              let token = JSON.stringify(data);
              sessionStorage.setItem(TOKEN_NAME, token);
              let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
              let decodedToken = decode(tk.access_token);
              
              this.menuService.listarPorUsuario(decodedToken["user_name"]).subscribe(data => {
                this.menuService.menuCambio.next(data);
              });
        
              this.router.navigate(["consulta"]);
            }
            else{
              this.mensaje=`Usuario y Contraseña invalido`;
            }
        }
  );   
  /*  .subscribe(data => {      
    if (data) {
      let token = JSON.stringify(data);
      sessionStorage.setItem(TOKEN_NAME, token);
      let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
      const decodedToken = decode(tk.access_token);
      
      this.menuService.listarPorUsuario('ricardo.mendez@econtinuum.cl').subscribe(data => {
        this.menuService.menuCambio.next(data);
      });

      this.router.navigate(["videotest"]);
    }
    else{
      this.mensaje=`Usuario y Contraseña invalido`;
    }
});*/
}
catch(error){
this.mensaje=`Usuario y Contraseña invalido`;
}
}

}


