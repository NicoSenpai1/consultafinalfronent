import { MedicoService } from './../../services/medico.service';
import { ActivatedRoute } from '@angular/router';
import { Medico } from './../../model/medico';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  cantidad:number;
  dataSource:MatTableDataSource<Medico>;
  displayedColumns=['id','run','nombres','direccion','telefono','email','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(public route:ActivatedRoute,private medicoService:MedicoService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.medicoService.medicoCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.medicoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    this.medicoService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
  eliminar(id:number){
    this.medicoService.eliminar(id).subscribe(data => {
      this.medicoService.listar().subscribe(data =>{
        this.medicoService.medicoCambio.next(data);
        this.medicoService.mensajeCambio.next('Se elimino');
      });
    });
  }
  /*mostarMas(e:any){
    console.log(e);
    return this.especialistaService.listarPageable(e.pageIndex,e.pageSize).subscribe(data => {
      let pacientes=JSON.parse(JSON.stringify(data)).content;
      this.dataSource=new MatTableDataSource(especialistas);
      this.cantidad=JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource.sort=this.sort;
    });
  }*/

}
