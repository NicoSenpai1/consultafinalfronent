import { MedicoService } from './../../../services/medico.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Medico } from '../../../model/medico';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Especialidad } from 'src/app/model/especialidad';
import { MedicoEspecialidad } from 'src/app/model/medicoespecialidad';
import { EspecialidadService } from 'src/app/services/especialidad.service';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-medico-edicion',
  templateUrl: './medico-edicion.component.html',
  styleUrls: ['./medico-edicion.component.css']
})
export class MedicoEdicionComponent implements OnInit {

  run:string='';
  nombres:string='';
  apellidos:string='';
  direccion:string='';
  telefono:string='';
  email:string='';
  especialidades:Especialidad[]=[];
  lista_especialidades:MedicoEspecialidad[]=[];
  filteredOptionsEspecialidad:Observable<any[]>;  
  especialidadSeleccionado:Especialidad;
  myControlEspecialidad:FormControl=new FormControl();
  form:FormGroup;
  mensaje:string;
  id:number;
  edicion:boolean=false;
  constructor(private builder:FormBuilder,private especialidadService:EspecialidadService,private snackBar:MatSnackBar,private medicoService:MedicoService,private route:Router,private router:ActivatedRoute) { 
    this.form=builder.group({
      'id':new FormControl(0),
      'especialidad':this.myControlEspecialidad,
      'nombres':new FormControl(''),
      'apellidos':new FormControl(''),
      'direccion':new FormControl(''),
      'telefono':new FormControl(''),      
      'email':new FormControl(''),      
      'run':new FormControl('')
      
    });
  }
  ngOnInit() {
    this.router.params.subscribe((params: Params)=>{
      this.id=params['id'];
      this.edicion=params['id']!=null;
      this.initForm();      
    });
    this.listarEspecialidad();
    this.filteredOptionsEspecialidad=this.myControlEspecialidad.valueChanges.pipe(map(val=>this.filterEspecialidad(val)));
  }
  initForm(){
    if(this.edicion){
      this.medicoService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'id':new FormControl(data.id),
          'especialidad':this.myControlEspecialidad,
          'nombres':new FormControl(data.nombres),
          'apellidos':new FormControl(data.apellidos),
          'direccion':new FormControl(data.direccion),
          'telefono':new FormControl(data.telefono),      
          'email':new FormControl(data.telefono),      
          'run':new FormControl(data.run),      
          
        });
        this.nombres=data.nombres;
        this.apellidos=data.apellidos;
        this.direccion=data.direccion;
        this.telefono=data.telefono;
        this.email=data.email;
        this.run=data.run;
        let i=0;
        
        
        for(i=0;i<data.especialidadmedicas.length;i++){
          let medico_especialidad=new MedicoEspecialidad();        
          medico_especialidad.especialidad=data.especialidadmedicas[i];
          medico_especialidad.medico=data;
          this.lista_especialidades.push(medico_especialidad);
          
        }
        /*medico_especialidad.especialidad=data.especialidadmedicas[0];
        medico_especialidad.medico=data;
        this.lista_especialidades.push(medico_especialidad);*/
          
        
        
      })
        
    }
  }
  listarEspecialidad(){
    return this.especialidadService.listar().subscribe(data=>{
      this.especialidades=data;
    });
  }
  displayFnEspecialidad(val:Especialidad){
    return val ? `${val.descripcion}` :val; 
  }
  
  seleccionarEspecialidad(e:any){
    this.especialidadSeleccionado=e.option.value;  
    console.log(e.option.value);
    
  }
  filterEspecialidad(val:any){
    if(val!=null && val.id>0){
      return this.especialidades.filter(opcion => 
        opcion.descripcion.toLowerCase().includes(val.descripcion.toLowerCase())
        );
    }
    else{
      return this.especialidades.filter(opcion => 
        opcion.descripcion.toLowerCase().includes(val.toLowerCase()) 
        );
    }
  }
  agregar(){
    let existe=false;
    if(this.especialidadSeleccionado!=null && this.especialidadSeleccionado.id>0){
      let i=0;
      for(i=0;i<this.lista_especialidades.length;i++){
        if(this.lista_especialidades[i].especialidad.id===this.especialidadSeleccionado.id){
          this.mensaje=`se encuentra en la lista`;
          this.snackBar.open(this.mensaje,"Aviso",{duration:2000});    
          existe=true;
          break;
  
        }
      }
    if(!existe){
      let det=new MedicoEspecialidad();
      det.especialidad=this.especialidadSeleccionado;
      this.lista_especialidades.push(det);
      this.especialidadSeleccionado=new Especialidad();
      this.myControlEspecialidad.setValue('');
    }
      
    }
    else{
      this.mensaje=`Debe agregar una Especialidad`;
      this.snackBar.open(this.mensaje,"Aviso",{duration:2000});
    }
  }
  validarDetalle(){}
  validarForm(){
    if(!this.edicion){
    return this.lista_especialidades.length===0 || this.nombres.length===0 || this.apellidos.length===0 || this.direccion.length===0 || this.telefono.length===0 || this.email.length===0 || this.run.length===0 ;
    }
  }
  operar(){
    console.log(this.form);
    let medico=new Medico();
    medico.run=this.run;
    medico.nombres=this.nombres;
    medico.apellidos=this.apellidos;
    medico.direccion=this.direccion;
    medico.telefono=this.telefono;
    medico.email=this.email;
    this.lista_especialidades.forEach(x=>medico.especialidadmedicas.push(x.especialidad));
    //console.log(producto);  
    if (!this.edicion){
    this.medicoService.registar(medico).subscribe(data=>{
      this.snackBar.open("Se registró","Aviso",{duration:2000});
      this.medicoService.listar().subscribe(medico => {this.medicoService.medicoCambio.next(medico);       
        this.medicoService.mensajeCambio.next("Guardado exitoso!");
    });
    
      setTimeout(()=>{
        this.limpiarControles();
      },2000);
      console.log(data);
      this.route.navigate(['medico']);
      
    });
  }
  else{
    medico.id=this.id;
    this.medicoService.modificar(medico).subscribe(data=>{
      this.snackBar.open("Se Actualizó","Aviso",{duration:2000});
      this.medicoService.listar().subscribe(medico => {this.medicoService.medicoCambio.next(medico);       
        this.medicoService.mensajeCambio.next("Guardado exitoso!");
    });
    
      setTimeout(()=>{
        this.limpiarControles();
      },2000);
      console.log(data);
      this.route.navigate(['medico']);
      
    });
  
  }
  }
  limpiarControles(){
    this.lista_especialidades=[];
    this.myControlEspecialidad.setValue('')
    this.nombres='';
    this.apellidos='';
    this.direccion='';
    this.telefono='';
    this.email='';
    this.run='';
  }
  removerDetalle(i :number){
    this.lista_especialidades.splice(i,1);
  
  }
}