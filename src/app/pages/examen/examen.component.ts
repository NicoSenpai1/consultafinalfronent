import { ExamenService } from './../../services/examen.service';
import { Examen } from './../../model/examen';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-examen',
  templateUrl: './examen.component.html',
  styleUrls: ['./examen.component.css']
})
export class ExamenComponent implements OnInit {

  dataSource:MatTableDataSource<Examen>;
  displayedColumns=['id','descripcion','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private examenService:ExamenService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.examenService.examenCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.examenService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    
    this.examenService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }
  eliminar(id:number){
    this.examenService.eliminar(id).subscribe(data => {
      this.examenService.listar().subscribe(data =>{
        this.examenService.examenCambio.next(data);
        this.examenService.mensajeCambio.next('Se elimino');
      });
    });
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
}

