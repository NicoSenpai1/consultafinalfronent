import { PacienteService } from './../../services/paciente.service';
import { Paciente } from './../../model/paciente';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  dataSource:MatTableDataSource<Paciente>;
  displayedColumns=['id','nombre','rut','email','telefono','sangre','fecha','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private pacienteService:PacienteService,private snackBar:MatSnackBar) { 

  }
  ngOnInit() {
    this.pacienteService.pacienteCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.pacienteService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    
    this.pacienteService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
        
    }
    eliminar(id:number){
      this.pacienteService.eliminar(id).subscribe(data => {
        this.pacienteService.listar().subscribe(data =>{
          this.pacienteService.pacienteCambio.next(data);
          this.pacienteService.mensajeCambio.next('Se elimino');
        });
      });
    }
    applyFilter(filterValue:string){
      filterValue=filterValue.trim();
      filterValue=filterValue.toLowerCase();
      this.dataSource.filter=filterValue;
    }
   

  }