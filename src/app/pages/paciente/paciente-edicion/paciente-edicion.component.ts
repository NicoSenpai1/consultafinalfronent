import { PacienteService } from './../../../services/paciente.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Paciente } from '../../../model/paciente';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {

  form:FormGroup;
  id:number;
  paciente:Paciente;
  edicion:boolean=false;
  constructor(private route:ActivatedRoute,private router:Router,private pacienteService:PacienteService) { 
    this.form=new FormGroup({
      'idPaciente': new FormControl(0),
      'nombrePaciente':new FormControl(''),
      'rutPaciente':new FormControl(''),
      'email':new FormControl(''),
      'numTelefono':new FormControl(''),   
      'tipoSangre':new FormControl(''),
      'fecha':new FormControl(0)
    }); 
  }
  initForm(){
    if(this.edicion){
      this.pacienteService.listarPorId(this.id).subscribe(data=>{
        this.form=new FormGroup({
          'idPaciente': new FormControl(data.idPaciente),
          'nombrePaciente':new FormControl(data.nombrePaciente),
          'rutPaciente':new FormControl(data.rutPaciente),
          'email':new FormControl(data.email),
          'numTelefono':new FormControl(data.numTelefono),
          'tipoSangre':new FormControl(data.tipoSangre),
          'fecha':new FormControl(data.fecha),
        });
      })
        
    }
  }
  ngOnInit() {
    this.paciente=new Paciente();
    this.route.params.subscribe((params: Params)=>{
      this.id=params['idPaciente'];
      this.edicion=params['idPaciente']!=null;
      this.initForm();      
    });
  }
  operar(){
    this.paciente.idPaciente=this.form.value['idPaciente'];
    this.paciente.nombrePaciente=this.form.value['nombrePaciente'];        
    this.paciente.rutPaciente=this.form.value['rutPaciente']; 
    this.paciente.email=this.form.value['email']; 
    this.paciente.numTelefono=this.form.value['numTelefono']; 
    this.paciente.tipoSangre=this.form.value['tipoSangre'];
    this.paciente.fecha=this.form.value['fecha'];

    if(this.edicion){

      this.pacienteService.modificar(this.paciente).subscribe(data=>
      {
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Se realizo el cambio de manera exitosa!");
      });
    });
    
    }
    else{
      this.pacienteService.registar(this.paciente).subscribe(data=>{
        this.pacienteService.listar().subscribe(pacientes => {this.pacienteService.pacienteCambio.next(pacientes);       
        this.pacienteService.mensajeCambio.next("Guardado exitoso!");
        });
      });
      
    }
    this.router.navigate(['paciente']);
  }


}
